class N4n0 < Formula
  desc "Nano is your social media marketing automation pal"
  homepage "https://gitlab.com/sales-army/nano"
  url "git@gitlab.com:sales-army/nano.git", :using => :git
  head "git@gitlab.com:sales-army/nano.git"
  version "latest"

  bottle :unneeded  
  depends_on :java => '10.0+'

  def install
    system "./gradlew", "-Pversion=#{version}", "shadowJar"
    libexec.install Dir['*']
    bin.write_jar_script libexec/"build/libs/nano-#{version}-all.jar", 'n4n0'
  end

  test do
    system "false"
  end
end